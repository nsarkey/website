title:      "Reading 00: Unix Shell, Git"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for **Friday, January 15** are:

    1. **[Unix Shell]**:

      The goal of this course is to get you familiar with the [Unix]
      programming environment.  In particular, we will be spending almost all
      of our time in a [Unix] shell.

      The first assignment is to read and work through [Command Line Crash
      Course], which is a tutorial into using the command line interface on
      various operating systems (focus on the [Unix] shell).

      Alternative, you may go through any of the following tutorials:

      - [Learning the Shell](http://linuxcommand.org/lc3_learning_the_shell.php)
      - [UNIX / Linux Tutorial for Beginners](http://www.ee.surrey.ac.uk/Teaching/Unix/)
      - [FreeBSD Handbook - UNIX Basics](https://www.freebsd.org/doc/handbook/basics.html)

  2. **[Git]**

      For this course, you will be using [Git] to keep track of your
      assignments and to submit your work.

      The second assignment is to read and work through [Pro
      Git](https://git-scm.com/book/en/v2), Chapters [1. Getting
      Started](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control)
      and [2. Git
      Basics](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository).

      Alternatively, you may go through any of the following tutorials:

      - [Atlassian Getting Started Git Tutorials](https://www.atlassian.com/git/tutorials)
      - [gittutorial](http://git-scm.com/docs/gittutorial)
      - [Code School - Try Git](https://try.github.io/levels/1/challenges/1)
      - [Learn Git Branching](http://pcottle.github.io/learnGitBranching/)
      - [Git - Tutorial](http://www.vogella.com/tutorials/Git/article.html)

  [Command Line Crash Course]: http://cli.learncodethehardway.org/book/
  [PuTTY]:  http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html
  [Git]:    https://git-scm.com
  [Unix Shell]: http://man7.org/linux/man-pages/man1/bash.1.html
  [Unix]:   https://en.wikipedia.org/wiki/Unix

  <div class="alert alert-info" markdown="1">
  #### <i class="fa fa-code"></i> Active Reading

  To get the most out of your reading, you should be **typing** commands into a
  terminal and **playing** around with the things you reading.

  </div>

  ## Questions

  Normally, you will have a couple of questions to answer for each reading.
  For this first assignment, however, you are to do the following:

  1. Create a [Bitbucket] account if you don't have one already.

  2. [Fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) the
  [CSE-20189-SP16/assignments] repository to create your own **private** repository.

      **Note**: Keep the name of the repository **assignments** and the original
      permissions.

      <div class="alert alert-danger" markdown="1">
      #### <i class="fa fa-lock"></i> Access Controls

      Please ensure that your repository is **Private** and that only
      **CSE-20189-SP16** and your account have access to your repository.

      To do this, as you go through forking process, make sure you check `This
      is a private repository` and `Inherit repository user/group permissions`.

      If you have already forked and forgot to mark these settings, you can fix
      your repository by doing the following:

      1. Click on the `Settings` (or Gear icon) link to go to the Settings page.

      2. Check the `This is a private repository` box and save the settings.

      3. Next, click on the `Access Management` link on the left.

      4. Finally, add the **CSE-20189-SP16** account and give it **Admin** access.

      Once you have done this then the repository is properly configured.

      </div>

  3. [Clone](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)
  your **private** repository to a machine such as your computer or one of the student machines.

      <div class="alert alert-warning" markdown="1">
      #### <i class="fa fa-thumbs-o-up"></i> SSH_ASKPASS workaround

      If you try to do a `git clone` on the **student** machines and it starts
      but fails to ask you for a password, try running this command first:

          unsetenv SSH_ASKPASS

      This will force `git` to use the terminal to ask you for your password
      rather than popup a window.

      </div>

  4. Edit the `README.md` file so that it has the appropriate `Name` and `NetID`.

  5. [Commit](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository) your work.

  6. [Push](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes) your work up to your **private** [Bitbucket] repository.

  Throughout this course, you will be using [Git] to submit your reading and
  homework assigments.

  [CSE-20189-SP16/assignments]: https://bitbucket.org/CSE-20189-SP16/assignments

  ## Commands

  In addition to writing responses to any question prompts, each reading
  assignment will require you to create short summaries ([tldr]) of some of the
  commands we will be discussing for that week.  Each command summary should
  provide a brief over of what the command does and then a succinct list of
  examples of commonly used applications of the command.  Moreover, a list of
  resources or references may be provided at the bottom of the summary page for
  further information.

  For instance the [CSE-20189-SP16/assignments] contains an example `git.md`
  file in the `reading00` folder.  To view this file, you can use the
  provided script:

      $ ./mdview.sh reading00/git.md

  This will process the [Markdown] flavored file and render it using [elinks]
  as shown below.

  <div class="thumbnail">
    <img src="static/img/ss-tldr.png" class="img-responsive"/>
  </div>

  You can also use the `tldr.sh` script to search for commands in the current
  directory :

      $ ./tldr.sh git

  This command will be useful once you have more summary pages and want to
  quickly grab the appropriate page for the corresponding command.

  For this assignment, you are to modify the `reading00/git.md` summary page by
  adding more example use cases (one example one is provided for you).

  In addition to [git], you should write at least **two** more summary pages
  for any of the following commands:

  * [id]
  * [who]
  * [whoami]
  * [groups]

  As with the [git] page, these additional pages should go in the `reading00`
  folder and have the extension `.md`.

  [id]:     http://man7.org/linux/man-pages/man1/id.1.html
  [who]:    http://man7.org/linux/man-pages/man1/who.1.html
  [whoami]: http://man7.org/linux/man-pages/man1/whoami.1.html
  [groups]: http://man7.org/linux/man-pages/man1/groups.1.html
  [tldr]:   https://en.wikipedia.org/wiki/Wikipedia:Too_long;_didn%27t_read
  [elinks]: http://elinks.or.cz/

  **Note**: The goal of these summary pages is to encourage you to build a set
    of notes and cheatsheets that will help you not only in this class, but in
    future endeavors as well.  Because of this, you should record any examples
    that you find useful or interesting.

  <div class="alert alert-info" markdown="1">
  #### <i class="fa fa-code"></i> Markdown

  The `README.md` and command summary files are annotated using [Markdown].
  This is a simple plain text formatting style that can be translated into
  HTML.

  For a more complete example of what you can do with [Markdown], check out the
  [tutorial provided by Bitbucket](https://bitbucket.org/tutorials/markdowndemo).  To view the result
  of the translation, you can use the provided `mdview.sh` script.

  </div>

  [Markdown]:   https://daringfireball.net/projects/markdown/

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.

  ## Submission

  To submit your response, please commit your work to the `reading00` folder in
  your **Assignments** [Bitbucket] repository.


  [Bitbucket]:    https://bitbucket.org
