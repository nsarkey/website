title:      "Reading 01: Files and Processes"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for **Monday, January 18** are:

  1. **Files**

      - [Chapter 3. About files and the file system](http://tldp.org/LDP/intro-linux/html/chap_03.html)

            You can skip the [About partitioning](http://tldp.org/LDP/intro-linux/html/sect_03_01.html#sect_03_01_02]) section.

  2. **Processes**

      - [Chapter 4. Processes](http://tldp.org/LDP/intro-linux/html/chap_04.html)

            You can skip [4.2. Boot process, Init and shutdown](http://tldp.org/LDP/intro-linux/html/sect_04_02.html).

  3. **Editors**:

      You should learn how to use a terminal-based text editor if you have not
      already.  Here are the most popular ones on Linux:

      1. **[Nano]**: This text editor is the easiest to get started with, but
        also has the least amount of features. It is what I recommend for
        newcomers to Linux.

          - [Using Nano](https://www.linode.com/docs/tools-reference/tools/using-nano)
          - [The Beginner's Guide to Nano](http://www.howtogeek.com/howto/42980/the-beginners-guide-to-nano-the-linux-command-line-text-editor/)
          - [The Nano Text Editor](http://mintaka.sdsu.edu/reu/nano.html)

      2. **[Vim]**: This text editor has many features such as syntax
        highlighting, plugins, and even spell checking. That said, it has a
        steep learning curve due to its foreign keyboard interface.

          - [CS 107 Guide to Vim](http://web.stanford.edu/class/cs107/guide_vim.html)
          - [Vim Introduction and Tutorial](https://blog.interlinked.org/tutorials/vim_tutorial.html)
          - [Interactive Vim Tutorial](http://www.openvim.com/tutorial.html)

      3. **[Emacs]**: This text editor also has many features (perhaps too
        many).  In fact, [Emacs] is so powerful, there is saying "Emacs is an
        excellent operating system.  All it's missing is a decent text editor".
        **Bazinga!**

          - [CS 107 Guide to Emacs](http://web.stanford.edu/class/cs107/guide_emacs.html)
          - [Absolute Beginner's Guide to Emacs](http://www.jesshamrick.com/2012/09/10/absolute-beginners-guide-to-emacs/)
          - [A Tutorial Introduction to GNU Emcas](http://www2.lib.uchicago.edu/keith/tcl-course/emacs-tutorial.html)

      You **don't** have to read all of the tutorials for each text editor
      (only read the ones for your weapon of choice).

  [Nano]:   http://www.nano-editor.org/
  [Vim]:    http://www.vim.org/
  [Emacs]:  https://www.gnu.org/software/emacs/

  ## Questions

  In your `reading01/README.md` file, describe what commands you would use to
  accomplish the following:

  1. Create a file `private` that is only readable by you.

  2. Create a link to `/afs/nd.edu/coursesp.16/cse/cse20189.01` in your home
  directory.

  3. Determine the size of a file named `BigFile`.

  4. Determine the size of a directory named `MyFolder`.

  5. Given the following output of `ps ux`:

          pbui     24532  0.0  0.0  36432  5312 pts/4    S+   23:17   0:00 ssh xavier
          pbui     25129  0.0  0.0  90040 15076 ?        S    23:48   0:00 urxvt
          pbui     25130  0.0  0.0  16260  3872 pts/8    Ss   23:48   0:00 -bash
          pbui     25263  0.0  0.0  38504  5224 pts/8    S+   23:48   0:00 ssh student00
          pbui     25361  0.0  0.0  36700  3352 pts/3    R+   23:51   0:00 ps ux
          pbui     25363  0.0  0.0   6088  1664 pts/3    S+   23:51   0:00 tail -n 10
          pbui     25809  0.0  0.0  16260  3312 pts/7    Ss+  Jan12   0:00 -bash
          pbui     25913  0.0  0.1  99792 25788 ?        S    Jan13   0:07 urxvt
          pbui     25914  0.0  0.0  16260  3952 pts/5    Ss   Jan13   0:00 -bash
          pbui     29626  0.0  0.0 176096  4524 ?        Sl   15:46   0:00 /usr/lib/dconf/dconf-service

      Terminate the `ssh student00` process.

  6. Given the output of `ps ux` above, how would you terminate all of the
  `urxvt` processes in one command?

  7. Determine how long a program named `simulation` takes to run.

  8. Persistently set the default your shell's `EDITOR` to your weapon of
  choice.

  ## Commands

  In the `reading01` folder, write at least **three** summary pages for the
  commands in each category:

  ### Files and Filesystem

  1. [ls](http://man7.org/linux/man-pages/man1/ls.1.html)

  2. [chmod](http://man7.org/linux/man-pages/man1/chmod.1.html)

  3. [stat](http://man7.org/linux/man-pages/man1/stat.1.html)

  4. [df](http://man7.org/linux/man-pages/man1/df.1.html)

  5. [du](http://man7.org/linux/man-pages/man1/du.1.html)

  6. [file](http://man7.org/linux/man-pages/man1/file.1.html)

  7. [ln](http://man7.org/linux/man-pages/man1/ln.1.html)

  8. [find](http://man7.org/linux/man-pages/man1/find.1.html)

  ### Processes

  1. [ps](http://man7.org/linux/man-pages/man1/ps.1.html)

  2. [kill](http://man7.org/linux/man-pages/man1/kill.1.html)

  3. [top](http://man7.org/linux/man-pages/man1/top.1.html)

  4. [time](http://man7.org/linux/man-pages/man1/time.1.html)

  5. [nice](http://man7.org/linux/man-pages/man1/nice.1.html)

  6. [crontab](http://man7.org/linux/man-pages/man5/crontab.5.html)

  7. [write](http://man7.org/linux/man-pages/man1/write.1.html)

  8. [mesg](http://man7.org/linux/man-pages/man1/mesg.1.html)

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.

  ## Submission

  To submit your assignment, please commit your work to the `reading01` folder
  in your **Assignments** [Bitbucket] repository.

  [Bitbucket]:    https://bitbucket.org
