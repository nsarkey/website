title:      "Reading 03: Developer Tools"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The focus of this week's readings are **developer tools**, that is
  applications that will help with programming C/C++ on a typical Unix system.

  The readings for **Monday, February 1** are:

  1. **Compiling and Building**:

      - [Makefiles: A tutorial by example](http://mrbook.org/blog/tutorials/make/)
      - [GCC and Make: Compiling, Linking and Building C/C++ Applications](https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html)

  2. **Debugging and Profiling**:

      - [Debugging with GDB](http://betterexplained.com/articles/debugging-with-gdb/)
      - [Valgrind Quick Start Guide](http://valgrind.org/docs/manual/quick-start.html)
      - [gprof Quick-Start Guide](http://web.eecs.umich.edu/~sugih/pointers/gprof_quick.html)
      - [Introduction to strace](http://www.redpill-linpro.com/sysadvent//2015/12/10/introduction-to-strace.html)

  **Optional**:

  1. **Compiling and Building**:
      - [Make](http://www.ploxiln.net/make.html)
      - [Advanced Makefile Tricks](http://www.cprogramming.com/tutorial/makefiles_continued.html)
      - [Managing Projects with GNU Make](http://www.oreilly.com/openbook/make3/book/index.csp)

  2. **Debugging and Profiling**:
      - [Guide to Faster, Less Frustrating Debugging](http://heather.cs.ucdavis.edu/~matloff/UnixAndC/CLanguage/Debug.html)
      - [GDB: A basic workflow](http://techblog.rosedu.org/gdb-a-basic-workflow.html)
      - [8 gdb tricks you should know](https://blogs.oracle.com/ksplice/entry/8_gdb_tricks_you_should)
      - [Valgrind introduction](http://techblog.rosedu.org/valgrind-introduction.html)
      - [5 simple ways to troubleshoot using Strace](http://hokstad.com/5-simple-ways-to-troubleshoot-using-strace)

  Also, please take a look at the manual pages linked below.

  ## Questions

  In your `reading03` folder write a simple `hello.c` program that prints
  "Hello, World".  Next create a `Makefile` that  compiles your `hello.c` with
  six different targets:

  1. `hello-dynamic`: Compiles `hello.c` into a dynamic executable

  2. `hello-static`: Compiles `hello.c` into a static executable

  3. `hello-debug`: Compiles `hello.c` into an executable with debugging symbols enabled

  4. `hello-profile`: Compiles `hello.c` into an executable with profiling information enabled

  5. `all`: Builds all of the `hello-*` executables (**this should be the default target**)

  6. `clean`: Removes all the `hello-*` executables

  In your `reading03/README.md` file, describe what command you would use to
  accomplish the following:

  1. View all the text in `ls`.

  2. Determine the which shared libraries `ls` requires.

  3. View all the system calls made during an invocation of `ls`.

  4. Debug the `hello-debug` program to fix errors.

  5. Check the `hello-dynamic` program for memory leaks or errors.

  6. Profile the `hello-profile` program to find any bottlenecks.

  ## Commands

  In the `reading03` folder, write at least **five** summary pages for the
  following commands (since some of the commands perform similar functions, only
  choose one from each set):

  1. [gcc](http://man7.org/linux/man-pages/man1/gcc.1.html)

  2. [strings](http://man7.org/linux/man-pages/man1/strings.1.html)

  3. [ldd](http://man7.org/linux/man-pages/man1/ldd.1.html)

  4. [ar](http://man7.org/linux/man-pages/man1/ar.1.html)

  5. [readelf](http://man7.org/linux/man-pages/man1/readelf.1.html)

  6. [make](http://man7.org/linux/man-pages/man1/make.1.html)

  7. [valgrind](http://man7.org/linux/man-pages/man1/valgrind.1.html)

  8. [gdb](http://man7.org/linux/man-pages/man1/gdb.1.html)

  9. [gprof](http://man7.org/linux/man-pages/man1/gprof.1.html)

  10. [strace](http://man7.org/linux/man-pages/man1/strace.1.html)

  <div class="alert alert-warning" markdown="1">
  #### <i class="fa fa-warning"></i> Summaries

  Note, your summaries should be in your own words and not simply copy and
  pasted from the manual pages.  They should be short and **concise** and only
  include **common** use cases.

  </div>

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.

  ## Submission

  To submit your assignment, please commit your work to the `reading03` folder
  in your **Assignments** [Bitbucket] repository by **the beginning of class**
  on **Monday, February 1**.

  [Bitbucket]:    https://bitbucket.org
