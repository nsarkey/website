#!/usr/bin/env python2

import csv
import StringIO
import subprocess
import time

TOTAL = 255.0

def percentage_to_letter(percentage):
    GRADES = (
        ('A' , 93),
        ('A-', 90),
        ('B+', 87),
        ('B' , 83),
        ('B-', 80),
        ('C+', 77),
        ('C' , 73),
        ('C-', 70),
        ('D' , 65),
        ('F' , 0),
    )
    for grade, bottom in GRADES:
        if percentage >= bottom:
            return grade
    return GRADES[-1][0]

def extract_field(d, k):
    return [float(d.get(f, '0.0') or '0.0') for f in d if k in f and 'Comment' not in f and 'EC' not in f]

for student in csv.DictReader(open('FA15_CSE_10101_CX_01.csv')):
    readings     = sum(extract_field(student, 'Reading'))
    quizzes      = sum(extract_field(student, 'Quiz'))
    exams        = sum(extract_field(student, 'Exam'))
    notebooks    = sum(list(sorted(extract_field(student, 'Notebook')))[1:])
    project      = sum(extract_field(student, 'Project'))
    extra_credit = sum(map(float, [student.get('Coding Movie (+) (0)', '0'), 
                                   student.get('Extra Credit (+) (R) (0)', '0'),
                                   student.get('OS Extra Credit (+)', '0') or '0',
                                   student.get('Notebook 10 EC (+) (0)', '0') or '0']))
    total        = sum([readings, quizzes, exams, notebooks, project, extra_credit])
    email        = StringIO.StringIO()
    email.write('''To: "{}" <{}@nd.edu> 
From: Peter Bui <pbui@nd.edu>
Cc: Peter Bui <pbui@nd.edu>
Subject: [CDT-30010-FA15] Grade Summary and Final Projection ({})

{}:

This is an automated email containing your current grade summary and a list of
projections for your final grade based on your Final Exam score.

If you have any questions or comments, please let me know.
'''.format(student['Student Name'],
           student['Student Id'],
           student['Student Id'],
           student['Student Name'].split(',')[-1].strip()))
    email.write('''
{} ({})
======================================

= CURRENT GRADE

Readings:     {} / 30
Quizzes:      {} / 60
Exams:        {} / 30
Notebooks:    {} / 120
Project:      {} / 15
Extra Credit: {}
--------------------------------------
Total:        {} ({:.2f}%)

= FINAL PROJECTIONS

'''.format(student['Student Name'],
           student['Student Id'],
           readings,
           quizzes,
           exams,
           notebooks,
           project,
           extra_credit,
           total,
           total*100.0/TOTAL))
    
    for points in 0, 10, 20, 30, 40:
        new_total  = total + points
        percentage = new_total*100.0 / 300.0

        email.write('{:2} / 45       {} ({:.2f}%) ({})\n'.format(points, new_total, percentage, percentage_to_letter(percentage)))

    print email.getvalue()

    p = subprocess.Popen(['msmtp', '-a', 'pbui@nd.edu', '--auto-from=on', '-t', '--', '{}@nd.edu'.format(student['Student Id'])], stdin=subprocess.PIPE)
    p.communicate(email.getvalue())[0]
    time.sleep(1)
